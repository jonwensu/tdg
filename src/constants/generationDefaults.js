export default {
    string: {
        length: 8,
        numeric: false,
        specialChars: false
    }
}