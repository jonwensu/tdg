import React, { Component } from 'react';
import {
        Button,
        TextField,
        Checkbox,
        FormControlLabel 
} from '@material-ui/core/';
import randomString from 'random-string';

import generationDefaults from '../../constants/generationDefaults';
import JSONPretty from 'react-json-pretty';
import 'react-json-pretty/JSONPretty.monikai.styl';
import Highlight from 'react-highlight';
import 'highlight.js/styles/vs2015.css';

export default class StringTestData extends Component {

    constructor() {
        super();

        const { length, numeric, specialChars } = generationDefaults.string;
        this.state = {
            length,
            numeric,
            specialChars,
            result: [],
            quantity: 1
        };
    }

    componentDidMount() {
    }

    handleChange = (field, prop) => {
        return event => {
            this.setState({
                [field]: event.target[prop]
            })
        }
    }

    generateData = () => {
        const { length, quantity, numeric, specialChars } = this.state;
        const result = Array(+quantity).fill().map((_) => {
            return randomString({ length, numeric, special: specialChars })
        });
        this.setState({  result: [] }, () => {
            this.setState({ result });
        })
    }

    render() {

        const { length, numeric, specialChars, quantity, result } = this.state;

        return (
            <div>
                <TextField
                    label="Length"
                    value={length}
                    onChange={this.handleChange('length', 'value')}
                    type="number"
                />
                <TextField
                    label="Quantity"
                    value={quantity}
                    onChange={this.handleChange('quantity', 'value')}
                    type="number"
                />
                <FormControlLabel
                    control={
                        <Checkbox
                            checked={numeric}
                            onChange={this.handleChange('numeric', 'checked')}
                            color="primary"
                        />
                    }
                    label='Include Numeric'
                />
                <FormControlLabel
                    control={
                        <Checkbox
                            checked={specialChars}
                            onChange={this.handleChange('specialChars', 'checked')}
                            color="primary"
                        />
                    }
                    label='Include Special Characters'
                />

                <Button onClick={this.generateData} variant='contained' color='primary'>Generate</Button>

                
                <div style={{ maxWidth: '50vw'}}>
                    {result.length > 0 && (
                        <Highlight language="javascript">
                            <div style={{ padding: 20  }}>
                                <JSONPretty id="json-pretty" json={result}></JSONPretty>
                            </div>
                        </Highlight>
                    )}
                </div>

            </div>
        )
    }
}
